package wallOfTweets;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Locale;
import java.util.Vector;
import java.util.stream.Stream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class WoTServlet
 */
@WebServlet("/")
public class WoTServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Locale currentLocale = new Locale("en");
	String ENCODING = "ISO-8859-1";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WoTServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String action = request.getServletPath();
		
		try {
			switch(action) {
				case "/delete":
					deleteTweet(request, response);
					break;
				case "/new":
					createTweet(request, response);
					break;
				default:
					returnPage(request, response);
					break;
			}			
		}

		catch (SQLException ex ) {
			throw new ServletException(ex);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	private void returnPage(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		Vector<Tweet> tweets = Database.getTweets();
		String accept = request.getHeader("Accept");
		if(accept.equals("text/plain")){
			printPLAINresult(tweets, request, response);
		}else{
			printHTMLresult(tweets, request, response);
		}
	}

	
	private void createTweet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		long id = -1;
		String author = request.getParameter("author");
		String text = request.getParameter("tweet_text");
		
		
		try {
			id = Database.insertTweet(author, text);
			String key = Long.toString(id);
			String value = getSHA512(Long.toString(id));
			Cookie cookie = new Cookie(key, value);
			
			response.addCookie(cookie);
			
			String accept = request.getHeader("Accept");
			if(accept.equals("text/plain")){
				response.setContentType("text/plain");
				response.setCharacterEncoding(ENCODING);
				PrintWriter out = response.getWriter();
				out.println(id);
				
			}else{
				response.sendRedirect(request.getContextPath());
			}
			
		} catch (SQLException e) {
			e.printStackTrace();			
		}
	
	}
	
	private void deleteTweet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String tweet_id = request.getParameter("tweet_id");
		Cookie[] cookies = request.getCookies();
		boolean isAuthor = false;
		
		if(cookies != null) {
			for(Cookie cookie : cookies) {
				if(getSHA512(tweet_id).equals(cookie.getValue())){
					isAuthor = true;
					deleteCookie(cookie.getName(), response);
				}		
			}	
		}
		
		if(isAuthor)
			Database.deleteTweet(Long.parseLong(tweet_id));
		
		response.sendRedirect(request.getContextPath());
	}
	
	private void deleteCookie(String name, HttpServletResponse response) {
		Cookie c = new Cookie(name, "");
		c.setMaxAge(0);
		response.addCookie(c);
	}

	private void printPLAINresult (Vector<Tweet> tweets, HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		res.setContentType("text/plain");
		res.setCharacterEncoding(ENCODING);
		PrintWriter out = res.getWriter();

		for (Tweet tweet: tweets) {
			out.print("tweet #" + tweet.getTwid() + ": " + tweet.getAuthor() + ": ");
			out.println(tweet.getText() + " [" + tweet.getDate() + "]");
		}


	}
	
	private void printHTMLresult (Vector<Tweet> tweets, HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
		DateFormat timeFormatter = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocale);
		res.setContentType ("text/html");
		res.setCharacterEncoding(ENCODING);
		PrintWriter  out = res.getWriter ( );
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<head><title>Wall of Tweets</title>");
		out.println("<link href=\"wallstyle.css\" rel=\"stylesheet\" type=\"text/css\" />");
		out.println("</head>");
		out.println("<body class=\"wallbody\">");
		out.println("<h1>Wall of Tweets</h1>");
		out.println("<div class=\"walltweet\">"); 
		out.println("<form method=\"post\" action=\"new\">");
		out.println("<table border=0 cellpadding=2>");
		out.println("<tr><td>Your name:</td><td><input name=\"author\" type=\"text\" size=70></td><td></td></tr>");
		out.println("<tr><td>Your tweet:</td><td><textarea name=\"tweet_text\" rows=\"2\" cols=\"70\" wrap></textarea></td>"); 
		out.println("<td><input type=\"submit\" name=\"action\" value=\"Tweet!\"></td></tr>"); 
		out.println("</table></form></div>");
		String currentDate = "None";
		for (Tweet tweet: tweets) {
			String messDate = dateFormatter.format(tweet.getDate());
			if (!currentDate.equals(messDate)) {
				out.println("<br><h3>...... " + messDate + "</h3>");
				currentDate = messDate;
			}
			out.println("<div class=\"wallitem\">");
			out.println("<h4><em>" + tweet.getAuthor() + "</em> @ "+ timeFormatter.format(tweet.getDate()) +"</h4>");
			out.println("<p>" + tweet.getText() + "</p>");
			out.println("<form method=\"post\" action=\"delete\">");
			out.println("<input type=\"hidden\" value="+tweet.getTwid()+" name=\"tweet_id\"></input>");
			out.println("<button type=\"submit\" name=\"Delete\">Elimina</button>"); 
			out.println("</form>");
			out.println("</div>");
		}
		out.println ( "</body></html>" );
	}
	
	private String getSHA512(String input){

		String toReturn = null;
		try {
		    MessageDigest digest = MessageDigest.getInstance("SHA-512");
		    digest.reset();
		    digest.update(input.getBytes("utf8"));
		    toReturn = String.format("%0128x", new BigInteger(1, digest.digest()));
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
		return toReturn;
    }
}
