package asw01cs;


import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
//This code uses the Fluent API

public class SimpleFluentClient {

	private static String URI = "http://localhost:8080/waslab01_ss/";

	public final static void main(String[] args) throws Exception {

		String tweet_id = insertaTweet();
		/* Insert code for Task #4 here */
    	System.out.println(Request.Get(URI).addHeader("Accept","text/plain").execute().returnContent());
    	
    	/* Insert code for Task #5 here */
    	
    	deleteTweet(tweet_id);
    	
    	System.out.println(Request.Get(URI).addHeader("Accept","text/plain").execute().returnContent());
    	
    }
	private static void deleteTweet(String tweet_id) throws ClientProtocolException, IOException {
		
		System.out.println(tweet_id);
		
		Request.Post(URI + "delete")
				.bodyForm(Form.form().add("tweet_id", tweet_id).build())
				.execute();
			
	}
	private static String insertaTweet(){
		try {
			Content res = Request.Post(URI + "new").addHeader("Accept","text/plain")
				.bodyForm(Form.form().add("author", "Pau").add("tweet_text", "aosnteuhsnaothu aosenutaoheu aoesntuhaos ").build())
				.execute().returnContent();

			return res.toString().trim();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}

